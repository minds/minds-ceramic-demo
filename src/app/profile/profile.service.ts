import { Injectable } from "@angular/core";
import { CeramicService } from "../common/ceramic.service";

export type ProfileType = {
    did: string;
    displayName: string;
    location: string;
}

@Injectable({ providedIn: 'root'})
export class ProfileService {
    constructor(private ceramicService: CeramicService) {}

    async getByDid(did: string): Promise<ProfileType> {
        const basicProfile = await this.ceramicService.didDataStore.get('basicProfile', did);

        return {
            did: did,
            displayName: basicProfile?.name,
            location: basicProfile?.homeLocation,
        };
    }
}