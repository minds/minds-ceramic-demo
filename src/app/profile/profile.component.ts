import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Subscription } from 'rxjs';
import { CeramicService } from '../common/ceramic.service';
import { ProfileService, ProfileType } from './profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

  did: string;
  profile: ProfileType;
  inProgress = false;
  isAuthenticatedSubscription: Subscription;

  constructor(
    private ceramicService: CeramicService,
    private profileService: ProfileService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.isAuthenticatedSubscription = combineLatest([this.ceramicService.isAuthenticated$, this.activatedRoute.params]).subscribe(([isAuthenticated, params]) => {
      if (isAuthenticated) {
        this.loadProfile((<any>params).did);
      }
    });
  }

  async loadProfile(did: string) {
    if (this.inProgress)
      return;
    this.inProgress = true;

    this.profile = await this.profileService.getByDid(did);

    this.inProgress = false;
  }

  ngOnDestroy(): void {
    this.isAuthenticatedSubscription?.unsubscribe();
  }

}
