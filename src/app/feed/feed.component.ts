import { Component, Input, OnInit } from '@angular/core';
import { CeramicService } from '../common/ceramic.service';
import { FeedService } from './feed.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {

  lastPostMessage$ = this.feedService.lastPostMessage$;

  constructor(private feedService: FeedService) { }

  ngOnInit(): void {
    this.feedService.loadFeed(this.did);
  }

  @Input()
  set did(did: string) {
    this.feedService.loadFeed(did);
  }

}
