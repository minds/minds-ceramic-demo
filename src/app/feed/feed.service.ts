import { Injectable } from "@angular/core";
import { ReplaySubject } from "rxjs";
import { CeramicService } from "../common/ceramic.service";

@Injectable({ providedIn: 'root'})
export class FeedService {
    lastPostMessage$: ReplaySubject<string> = new ReplaySubject();

    constructor(private ceramicService: CeramicService) {

    }

    /**
     * Will load the latest post a user has made
     * @param did
     */
    async loadFeed(did: string): Promise<void> {
        const streamId = await this.ceramicService.didDataStore.get('myNote', did);

        this.lastPostMessage$.next(streamId.text);
    }
}