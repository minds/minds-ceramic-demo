import { Component, OnInit } from '@angular/core';
import { CeramicService } from '../common/ceramic.service';

@Component({
  selector: 'app-did',
  templateUrl: './did.component.html',
  styleUrls: ['./did.component.scss']
})
export class DidComponent implements OnInit {

  inProgress = false;
  did: string;

  name: string;
  location: string;

  constructor(private ceramicService: CeramicService) { }

  ngOnInit(): void {
    //this.onAuth();
  }

  async onAuth() {
    if (this.inProgress)
      return;
    this.inProgress = true;
    try {
      const did = await this.ceramicService.authenticate();
        this.did =  did.id;

        const basicProfile = await this.ceramicService.didDataStore.get('basicProfile');

        this.name = basicProfile.name;
        this.location = basicProfile.homeLocation;
    } catch(e) {
      alert(e);
    } finally {
      this.inProgress = false;
    }
  }

}
