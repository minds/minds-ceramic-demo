import { Injectable } from "@angular/core";

import CeramicClient from '@ceramicnetwork/http-client';
import { getResolver as getKeyResolver } from 'key-did-resolver'
import { getResolver as get3IDResolver } from '@ceramicnetwork/3id-did-resolver'
import { ThreeIdConnect, EthereumAuthProvider } from '@3id/connect'
import { DID } from 'dids';


import modelAliases from '../../dev-tools/model.json'
import { DataModel } from "@glazed/datamodel";
import { DIDDataStore } from "@glazed/did-datastore";
import { Resolver } from "did-resolver";

import { isCAIP10string } from "@self.id/core";
import { Caip10Link } from "@ceramicnetwork/stream-caip10-link";
import { BehaviorSubject } from "rxjs";

import Web3Modal from 'web3modal';
import { Web3Provider } from "@ethersproject/providers";
// import WalletConnectProvider from "@walletconnect/web3-provider";


export const API_URL = 'https://ceramic.minds.io';


@Injectable({ providedIn: 'root' })
export class CeramicService {

    ceramic: CeramicClient;
    resolver: Resolver;
    dataModel: DataModel<typeof modelAliases>;
    didDataStore: DIDDataStore;

    did: DID;

    threeIdConnect: ThreeIdConnect;

    isConnected = false;

    isAuthenticated$: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor() {
        this.ceramic = new CeramicClient(API_URL);

        this.dataModel = new DataModel({
            autopin: true,
            ceramic: this.ceramic,
            model: modelAliases
        });

        this.didDataStore = new DIDDataStore({ 
            autopin: true,
            ceramic: this.ceramic, 
            model: this.dataModel
        });

        this.resolver = new Resolver({ 
            ...getKeyResolver(),
            ...get3IDResolver(this.ceramic)
        })

        //

        this.threeIdConnect = new ThreeIdConnect();
    }

    async connect(authProvider: EthereumAuthProvider): Promise<DID> {
        this.ceramic.did = new DID({
            resolver: this.resolver
        });

        await this.threeIdConnect.connect(authProvider);

        const provider = await this.threeIdConnect.getDidProvider();

        this.ceramic.did.setProvider(provider);

        return this.ceramic.did;
    }


    async authenticate(): Promise<DID> {
        let web3Modal = new Web3Modal({
            network: 'mainnet',
            cacheProvider: true,
            providerOptions: {
                // walletconnect: {
                //     package: WalletConnectProvider,
                //     // options: {
                //     //   infuraId: process.env.REACT_APP_INFURA_ID
                //     // }
                //   },
            }
          });

        const modalProvider = await web3Modal.connect();
        const ethersProvider = new Web3Provider(modalProvider);

        const accounts = await (<any>ethersProvider.provider).enable();

        const authProvider = new EthereumAuthProvider(ethersProvider.provider, accounts[0]);

        const did = await this.connect(authProvider);

        await did.authenticate();

        this.isAuthenticated$.next(true);

        return did;
    }

    async getAccountDID(account: string): Promise<string> {
        const link = await Caip10Link.fromAccount(this.ceramic, account)
        if (link.did == null) {
          throw new Error(`No DID found for ${account}`)
        }
        console.log(link, link.did);
        return link.did
      }

    async toDID(accountOrDID: string): Promise<string> {
        console.log(accountOrDID, isCAIP10string(accountOrDID));
        return isCAIP10string(accountOrDID) ? await this.getAccountDID(accountOrDID) : accountOrDID
    }

}