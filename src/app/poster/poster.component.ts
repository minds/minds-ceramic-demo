import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CeramicService } from '../common/ceramic.service';
import { FeedService } from '../feed/feed.service';

@Component({
  selector: 'app-poster',
  templateUrl: './poster.component.html',
  styleUrls: ['./poster.component.scss']
})
export class PosterComponent implements OnInit {

  formGroup: FormGroup;
  isPosting = false;

  currentPostMessage: string;

  constructor(private ceramic: CeramicService, fb: FormBuilder, private feedService: FeedService) {
    this.formGroup = fb.group({
      message: ['']
    })
   }

  ngOnInit(): void {

  }


  async onPost(e: MouseEvent) {
    this.isPosting = true;

    const text = this.formGroup.get('message').value;

    //console.log(await this.ceramic.dataModel.createTile('SimpleNote', { text}));
    
    const streamId = await this.ceramic.didDataStore.set('myNote', { text });

    this.isPosting = false;

    this.currentPostMessage = text;

    this.formGroup.reset();

    console.log("posted id: " + streamId.toString());

    this.feedService.lastPostMessage$.next(text);
  }

}
