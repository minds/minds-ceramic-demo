import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CeramicService } from '../common/ceramic.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit, OnDestroy {

  isAuthenticatedSubscription: Subscription;
  isAuthenticated$ = this.ceramicService.isAuthenticated$;
  activeDid: string;

  constructor(protected ceramicService: CeramicService) { }

  ngOnInit(): void {
    this.isAuthenticatedSubscription = this.ceramicService.isAuthenticated$.subscribe(isAuthenticated => {
      if (isAuthenticated) {
        this.activeDid = this.ceramicService.ceramic.did.id;
      }
    })
  }

  ngOnDestroy(): void {
    this.isAuthenticatedSubscription?.unsubscribe();
  }

}
